section .text
         global _start
         _start:
           jmp short dummy        ; 1.
   
         imprimir_str:            ; 3.
           xor eax,eax            ; eax = 0
           pop ecx                ; ecx => "you win!A"
           mov [ecx+8],al         ; ecx => "you win!\0"
           mov al,4               ; syscall write: #4
           xor ebx,ebx            ; ebx = 0
           inc ebx                ; stdout filedescriptor: #1
           xor edx,edx            ; edx = 0
           mov dl,9               ; longitud "you win!\0": 9
           int 0x80               ; write(1, string, 9)
           
           mov al,1               ; syscall exit: #1
           dec ebx                ; ebx = 0
           int 0x80               ; exit(0)
       
         dummy:                   ; 2.
           call imprimir_str      ; apilo addr "you win!A"
           db 'you win!A'
